import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
/* eslint-disable no-unused-vars */
import ContactUs from '@/components/ContactUs'
import BuyNow from '@/components/BuyNow'
import ProductFunction from '@/components/ProductFunction'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/ContactUs',
      name: 'ContactUs',
      component: ContactUs
    },
    {
      path: '/BuyNow',
      name: 'BuyNow',
      component: BuyNow
    },
    {
      path: '/ProductFunction',
      name: 'ProductFunction',
      component: ProductFunction
    }
  ]
})
